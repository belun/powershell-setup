﻿$tokenColors = @{ 

    "None" = "Red";
    "Comment" = "Black";

    "Keyword" = "Black";
    "Command" = "DarkCyan";
    "Parameter" = "Black";
    
    "Operator" = "Gray";    
    "String" = "Gray";    
    "Number" = "Gray";

    "Type" = "DarkGreen";
    "Variable" = "DarkGreen";
    "Member" = "DarkYellow";    
}

$tokenColors2 = @{ 
    "ContinuationPromptForegroundColor" = "Gray";
    "ContinuationPromptBackgroundColor" = "DarkGray";
    "EmphasisForegroundColor" = "DarkGreen";
    "EmphasisBackgroundColor" = "Gray";
    "ErrorForegroundColor" = "DarkRed";
    "ErrorBackgroundColor" = "DarkGray";
}

$outputColors = @{ 
    "ProgressForegroundColor" = "Gray";
    "ProgressBackgroundColor" = "DarkGray";
    "VerboseForegroundColor" = "DarkYellow";
    "VerboseBackgroundColor" = "DarkGray";
    "DebugForegroundColor" = "DarkGreen";
    "DebugBackgroundColor" = "DarkGray";
    "WarningForegroundColor" = "DarkCyan";
    "WarningBackgroundColor" = "DarkGray";
    "ErrorForegroundColor" = "Red";
    "ErrorBackgroundColor" = "DarkGray";
}

foreach ($token in $tokenColors.Keys) {
    Write-Host "${token}: $($tokenColors[$token])"
    Set-PSReadlineOption -TokenKind $token -ForegroundColor $($tokenColors[$token]) -BackgroundColor DarkGray
}

foreach ($token in $tokenColors2.Keys) {
    Write-Host "${token}: $($tokenColors2[$token])"
}
Set-PSReadlineOption @tokenColors2

foreach ($outputColor in $outputColors.Keys) {
    Write-Host "${outputColor}: $($outputColors[$outputColor])"
    $host.PrivateData.$outputColor =  $($outputColors[$outputColor])
}
