New-PSDrive -Name dev -PSProvider FileSystem -Root "D:\'development"

function dev { Set-Location dev: }

function Prompt { 
    $location = pwd 


    $day = $(Get-Date -format "ddd dd") 
    $month = $(Get-Date -format "MMMM MM")
    $time = $(Get-Date -format "hh:mm:sstt")

    $sample = "[$(pwd)|$(Get-Date -format "ddd dd(MMMM MM)-hh:mm:sstt")>> "
    
    Write-Host "[" -NoNewline -ForegroundColor (Get-PSReadlineOption).OperatorForegroundColor
    Write-Host $location -NoNewline -ForegroundColor (Get-PSReadlineOption).CommentForegroundColor
    Write-Host "|" -NoNewline -ForegroundColor (Get-PSReadlineOption).OperatorForegroundColor
    Write-Host $day -NoNewline -ForegroundColor (Get-PSReadlineOption).CommandForegroundColor
    Write-Host "(" -NoNewline -ForegroundColor (Get-PSReadlineOption).OperatorForegroundColor
    Write-Host $month -NoNewline -ForegroundColor (Get-PSReadlineOption).MemberForegroundColor
    Write-Host ")" -NoNewline -ForegroundColor (Get-PSReadlineOption).OperatorForegroundColor
    Write-Host $time -NoNewline -ForegroundColor (Get-PSReadlineOption).VariableForegroundColor
    Write-Host ">>" -NoNewline -ForegroundColor (Get-PSReadlineOption).OperatorForegroundColor
    return " ";
}